/* 
   #NAME# - #DESCRIPTION#

   Copyright (C) #YEAR# #AUTHOR#

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

#include <termios.h>
#include <grp.h>
#include <pwd.h>
*/

#include <stdio.h>
#include <sys/types.h>
#include <getopt.h>
#include "system.h"

#define EXIT_FAILURE 1

char *xmalloc ();
char *xrealloc ();
char *xstrdup ();


static void usage (int status);

/* The name the program was run with, stripped of any leading path. */
char *program_name;

/* getopt_long return codes */
enum {DUMMY_CODE=129
@brief@
      ,BRIEF_CODE
@@
@dry-run@
      ,DRYRUN_CODE
@@
@no-warn@
      ,NOWARN_CODE
@@
@cd@
      ,CD_CODE
@@
@directory@
      ,DIRECTORY_CODE
@@
};

/* Option flags and variables */

@output@
char *oname = "stdout";		/* --output */
FILE *ofile;
@@
@cd@
char *new_directory;		/* --cd */
@@
@directory@
char *desired_directory;	/* --directory */
@@
@interactive@
int want_interactive;		/* --interactive */
@@
@quiet@
int want_quiet;			/* --quiet, --silent */
@@
@brief@
int want_brief;			/* --brief */
@@
@verbose@
int want_verbose;		/* --verbose */
@@
@dry-run@
int want_dry_run;		/* --dry-run */
@@
@no-warn@
int want_no_warn;		/* --no-warn */
@@

static struct option const long_options[] =
{
@interactive@
  {"interactive", no_argument, 0, 'i'},
@@
@output@
  {"output", required_argument, 0, 'o'},
@@
@quiet@
  {"quiet", no_argument, 0, 'q'},
  {"silent", no_argument, 0, 'q'},
@@
@brief@
  {"brief", no_argument, 0, BRIEF_CODE},
@@
@verbose@
  {"verbose", no_argument, 0, 'v'},
@@
@dry-run@
  {"dry-run", no_argument, 0, DRYRUN_CODE},
@@
@no-warn@
  {"no-warn", no_argument, 0, NOWARN_CODE},
@@
@cd@
  {"cd", required_argument, 0, CD_CODE},
@@
@directory@
  {"directory", required_argument, 0, DIRECTORY_CODE},
@@
  {"help", no_argument, 0, 'h'},
  {"version", no_argument, 0, 'V'},
  {NULL, 0, NULL, 0}
};

static int decode_switches (int argc, char **argv);

int
main (int argc, char **argv)
{
  int i;

  program_name = argv[0];

  i = decode_switches (argc, argv);

  /* do the work */

  exit (0);
}

/* Set all the option flags according to the switches specified.
   Return the index of the first non-option argument.  */

static int
decode_switches (int argc, char **argv)
{
  int c;

@output@
  ofile = stdout;
@@

  while ((c = getopt_long (argc, argv, 
@interactive@
			   "i"	/* interactive */
@@
@quiet@
			   "q"	/* quiet or silent */
@@
@verbose@
			   "v"	/* verbose */
@@
@output@
			   "o:"	/* output */
@@
			   "h"	/* help */
			   "V",	/* version */
			   long_options, (int *) 0)) != EOF)
    {
      switch (c)
	{
@interactive@
	case 'i':		/* --interactive */
	  want_interactive = 1;
	  break;
@@
@output@
	case 'o':		/* --output */
	  oname = xstrdup(optarg);
	  ofile = fopen(oname, "w");
	  if (!ofile)
	    {
	      fprintf(stderr, "cannot open %s for writing", optarg);
	      exit(1);
	    }
	  break;
@@
@quiet@
	case 'q':		/* --quiet, --silent */
	  want_quiet = 1;
	  break;
@@
@brief@
	case BRIEF_CODE:	/* --brief */
	  want_brief = 1;
	  break;
@@
@verbose@
	case 'v':		/* --verbose */
	  want_verbose = 1;
	  break;
@@
@dry-run@
	case DRYRUN_CODE:	/* --dry-run */
	  want_dry_run = 1;
	  break;
@@
@no-warn@
	case NOWARN_CODE:	/* --no-warn */
	  want_no_warn = 1;
	  break;
@@
@cd@
	case CD_CODE:		/* --cd */
	  new_directory = xstrdup(optarg);
	  break;
@@
@directory@
	case DIRECTORY_CODE:	/* --directory */
	  desired_directory = xstrdup(optarg);
	  break;
@@
	case 'V':
	  printf ("#NAME# %s\n", VERSION);
	  exit (0);

	case 'h':
	  usage (0);

	default:
	  usage (EXIT_FAILURE);
	}
    }

  return optind;
}


static void
usage (int status)
{
  printf (_("%s - \
#DESCRIPTIONC#\n"), program_name);
  printf (_("Usage: %s [OPTION]... [FILE]...\n"), program_name);
  printf (_("\
Options:\n\
@output@
  -o, --output NAME          send output to NAME instead of standard output\n\
@@
@interactive@
  -i, --interactive          prompt for confirmation\n\
@@
@dry-run@
  --dry-run                  take no real actions\n\
@@
@no-warn@
  --no-warn                  disable warnings\n\
@@
@quiet@
  -q, --quiet, --silent      inhibit usual output\n\
@@
@brief@
  --brief                    shorten output\n\
@@
@verbose@
  --verbose                  print more information\n\
@@
@directory@
  --directory NAME           use specified directory\n\
@@
@cd@
  --cd NAME                  change to specified directory before proceeding\n\
@@
  -h, --help                 display this help and exit\n\
  -V, --version              output version information and exit\n\
"));
  exit (status);
}
