/* 
   #NAME# - #DESCRIPTION#

   Copyright (C) #YEAR# #AUTHOR#

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

#include <termios.h>
#include <grp.h>
#include <pwd.h>
*/

#include <stdio.h>
#include <sys/types.h>
#include <argp.h>
#include "system.h"

#define EXIT_FAILURE 1

#if ENABLE_NLS
# include <libintl.h>
# define _(Text) gettext (Text)
#else
# define textdomain(Domain)
# define _(Text) Text
#endif
#define N_(Text) Text

char *xmalloc ();
char *xrealloc ();
char *xstrdup ();

static error_t parse_opt (int key, char *arg, struct argp_state *state);
static void show_version (FILE *stream, struct argp_state *state);

/* argp option keys */
enum {DUMMY_KEY=129
@brief@
      ,BRIEF_KEY
@@
@dry-run@
      ,DRYRUN_KEY
@@
@no-warn@
      ,NOWARN_KEY
@@
@cd@
      ,CD_KEY
@@
@directory@
      ,DIRECTORY_KEY
@@
};

/* Option flags and variables.  These are initialized in parse_opt.  */

@output@
char *oname;			/* --output=FILE */
FILE *ofile;
@@
@cd@
char *new_directory;		/* --cd=DIRECTORY */
@@
@directory@
char *desired_directory;	/* --directory=DIR */
@@
@interactive@
int want_interactive;		/* --interactive */
@@
@quiet@
int want_quiet;			/* --quiet, --silent */
@@
@brief@
int want_brief;			/* --brief */
@@
@verbose@
int want_verbose;		/* --verbose */
@@
@dry-run@
int want_dry_run;		/* --dry-run */
@@
@no-warn@
int want_no_warn;		/* --no-warn */
@@

static struct argp_option options[] =
{
@interactive@
  { "interactive", 'i',           NULL,            0,
    N_("Prompt for confirmation"), 0 },
@@
@output@
  { "output",      'o',           N_("FILE"),      0,
    N_("Send output to FILE instead of standard output"), 0 },
@@
@quiet@
  { "quiet",       'q',           NULL,            0,
    N_("Inhibit usual output"), 0 },
  { "silent",      0,             NULL,            OPTION_ALIAS,
    NULL, 0 },
@@
@brief@
  { "brief",       BRIEF_KEY,     NULL,            0,
    N_("Shorten output"), 0 },
@@
@verbose@
  { "verbose",     'v',           NULL,            0,
    N_("Print more information"), 0 },
@@
@dry-run@
  { "dry-run",     DRYRUN_KEY,    NULL,            0,
    N_("Take no real actions"), 0 },
@@
@no-warn@
  { "no-warn",     NOWARN_KEY,    NULL,            0,
    N_("Disable warnings"), 0 },
@@
@cd@
  { "cd",          CD_KEY,        N_("DIRECTORY"), 0,
    N_("Change to DIRECTORY before proceeding"), 0 },
@@
@directory@
  { "directory",   DIRECTORY_KEY, N_("DIR"),       0,
    N_("Use directory DIR"), 0 },
@@
  { NULL, 0, NULL, 0, NULL, 0 }
};

/* The argp functions examine these global variables.  */
const char *argp_program_bug_address = "<#EMAIL#>";
void (*argp_program_version_hook) (FILE *, struct argp_state *) = show_version;

static struct argp argp =
{
  options, parse_opt, N_("[FILE...]"),
  N_("#DESCRIPTIONC#"),
  NULL, NULL, NULL
};

int
main (int argc, char **argv)
{
  textdomain(PACKAGE);
  argp_parse(&argp, argc, argv, 0, NULL, NULL);

  /* TODO: do the work */

  exit (0);
}

/* Parse a single option.  */
static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  switch (key)
    {
    case ARGP_KEY_INIT:
      /* Set up default values.  */
@output@
      oname = "stdout";
      ofile = stdout;
@@
@cd@
      new_directory = NULL;
@@
@directory@
      desired_directory = NULL;
@@
@interactive@
      want_interactive = 0;
@@
@quiet@
      want_quiet = 0;
@@
@brief@
      want_brief = 0;
@@
@verbose@
      want_verbose = 0;
@@
@dry-run@
      want_dry_run = 0;
@@
@no-warn@
      want_no_warn = 0;
@@
      break;

@interactive@
    case 'i':			/* --interactive */
      want_interactive = 1;
      break;
@@
@output@
    case 'o':			/* --output */
      oname = xstrdup (arg);
      ofile = fopen (oname, "w");
      if (!ofile)
	argp_failure (state, EXIT_FAILURE, errno,
		      _("Cannot open %s for writing"), oname);
      break;
@@
@quiet@
    case 'q':			/* --quiet, --silent */
      want_quiet = 1;
      break;
@@
@brief@
    case BRIEF_KEY:		/* --brief */
      want_brief = 1;
      break;
@@
@verbose@
    case 'v':			/* --verbose */
      want_verbose = 1;
      break;
@@
@dry-run@
    case DRYRUN_KEY:		/* --dry-run */
      want_dry_run = 1;
      break;
@@
@no-warn@
    case NOWARN_KEY:		/* --no-warn */
      want_no_warn = 1;
      break;
@@
@cd@
    case CD_KEY:		/* --cd */
      new_directory = xstrdup (optarg);
      break;
@@
@directory@
    case DIRECTORY_KEY:		/* --directory */
      desired_directory = xstrdup (optarg);
      break;
@@

    case ARGP_KEY_ARG:		/* [FILE]... */
      /* TODO: Do something with ARG, or remove this case and make
         main give argp_parse a non-NULL fifth argument.  */
      break;

    default:
      return ARGP_ERR_UNKNOWN;
    }
  return 0;
}

/* Show the version number and copyright information.  */
static void
show_version (FILE *stream, struct argp_state *state)
{
  (void) state;
  /* Print in small parts whose localizations can hopefully be copied
     from other programs.  */
  fputs(PACKAGE" "VERSION"\n", stream);
  fprintf(stream, _("Written by %s.\n\n"), "#AUTHOR#");
  fprintf(stream, _("Copyright (C) %s %s\n"), "#YEAR#", "#AUTHOR#");
  fputs(_("\
This program is free software; you may redistribute it under the terms of\n\
the GNU General Public License.  This program has absolutely no warranty.\n"),
	stream);
}
