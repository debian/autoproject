.\"                              hey, Emacs:   -*- nroff -*-
.\" autoproject is free software; you can redistribute it and/or modify
.\" it under the terms of the GNU General Public License as published by
.\" the Free Software Foundation; either version 2 of the License, or
.\" (at your option) any later version.
.\"
.\" This program is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.\" GNU General Public License for more details.
.\"
.\" You should have received a copy of the GNU General Public License
.\" along with this program; see the file COPYING.  If not, write to
.\" the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
.\"
.TH AUTOPROJECT 1 "September 16, 2002" "" "Programmer's Manual"
.\" Please update the above date whenever this man page is modified.
.SH NAME
autoproject \- create a skeleton source package for a new program
.SH SYNOPSIS
.B autoproject
.RI " [" options "] [" name "] "
.SH DESCRIPTION
\fBautoproject\fP simplifies the creation of a source package for a
new program.
The idea is that you execute \fBautoproject\fP just once when you start a
new project.  It will create a new directory
and populate it with standard files, customized for the new project.
.PP
\fBautoproject\fP asks for the name of the new program (unless it is
given on the command line), a program description and other data. It
then creates a subdirectory and populates it with a C program with
command line parsing, a simple manual page and texinfo page, and other
standard files.  The package is intended to follow the GNU programming
standards.  It uses \fBautoconf\fP(1) to configure itself, and
\fBautomake\fP(1) to create the Makefile.
.PP
The new program will always support the options "--help" and
"--version", and will optionally support certain standard options such
as "--quiet".
.PP
Optionally, \fBautoproject\fP can set the new project up to use the
argp command line parsing interface that is included in GNU C library
2.1.
.PP
Alternatively, the new project can use a
command line parser generator.  \fBautoproject\fP will create an
appropriate options description file, and the generated \fIMakefile\fP
will include the commands to invoke the parser generator as needed.
Currently, \fBautogen\fP(1) and \fBclig\fP(1) are supported.
.PP
At present \fBautoproject\fP supports main programs only
in c, sh, or c++.  For other languages mentioned, \fBautoproject\fP
only adds macros in \fIconfigure.in\fP so \fBautoconf\fP(1) will look
for the relevant compilers.  (You may add skeleton files supporting
other languages.  See \fBCUSTOMIZATION\fP, below.)
.PP
The version number for the new program is initialized as 0.1.0, and is
set in configure.in (only).  It is available in C programs as the macro
VERSION, and in the Makefile as $(VERSION).
.PP
If, after populating the new directory, there exists an executable
file named \fIpostinst\fP, then it is executed.  If it executes
successfully, then \fBautoproject\fP deletes it.  Currently,
\fBautoproject\fP does not supply a file \fIpostinst\fP.  However, a
user can install one to perform any necessary actions.  (See
\fBCUSTOMIZATION\fP, below.)
.SH OPTIONS
If the GNU version of \fBgetopt\fP(1) is installed, \fBautoproject\fP
will accept the following options.  Otherwise, \fBautoproject\fP will
use \fBgetopts\fP(1) to parse its arguments, and it will not longer
accept long options or options with optional arguments.  If
\fBautoproject\fP is used to generate a shell-based project, it will still
be dependent on GNU \fBgetopt\fP.
.TP
.BR -a , " --author \fIname\fP"
Supply the name of the new program's author.
.TP
.BR -e , " --email \fIaddr\fP"
Supply the email address of the author.
.TP
.BR -o , " --option \fIopt\fP"
Add \fIopt\fP to the list of long options accepted by the program.
Only these standard options are accepted here: 
\fBdry-run no-warn output brief quiet verbose directory cd
interactive\fP.
.TP
.BR -d , " --description \fItext\fP"
Supply the short program description
.TP
.BR -i , " --interface \fItype\fP"
Specify the type of user interface.  The default is \fBcli\fP, for
command line interface.  (Currently, only \fBcli\fP is supported.)
.TP
.BR -l , " --language \fIlang\fP"
Add \fIlang\fP to the list of languages used.  These languages are
supported to some extent: \fBc sh c++ fortran lex yacc awk\fP.
\fBautoproject\fP supports languages in two ways.  It assumes the
first language mentioned will be used for the main program, and
searches for a skeleton program file in the corresponding section of
the library.  At present \fBautoproject\fP supports main programs only
in c, sh, or c++.  For other languages mentioned, \fBautoproject\fP
only adds macros in \fIconfigure.in\fP so \fBautoconf\fP(1) will look
for the relevant compilers.  (You may add skeleton files supporting
other languages.  See \fBCUSTOMIZATION\fP, below.)
.TP
.BR -L [\fIDIR\fP]
Prepend \fIDIR\fP to the list of directories to search for skeleton
files.  (See \fBCUSTOMIZATION\fP, below.)
If \fIDIR\fP is missing, then the path is cleared.
.TP
.BR -n , " --name \fIname\fP"
Specify the name of the new program.
.TP
.BR -p , " --parser \fIprog\fP"
Use the external command line parser or parser generator \fIprog\fP.
Currently, these are supported: \fBargp\fP, \fBautogen\fP(1) and
\fBclig\fP(1).
.TP
.BR --debug
Leave intermediate files.
.TP
.BR \-h , " \-\-help"
Show summary of options.
.TP
.BR \-v , " \-\-version"
Show version of program.
.SH CUSTOMIZATION
The \fBautoproject\fP package includes a set of skeleton files which
are usually installed under \fI/usr/local/share/autoproject\fP.  It
selects which subdirectories to use based on the interface type,
primary language, and parser generator chosen by the user.  
.PP
The user may create a similar directory tree under
\fI$HOME/.autoproject\fP, and populate it with additional files and/or
replacements for the standard files.
The system administrator may create a similar tree under
\fI/etc/autoproject\fP.  \fBautoproject\fP searches in
\fI$HOME/.autoproject\fP first, then \fI/etc/autoproject\fP, and
finally in the standard tree.  It uses only the first file it finds of
a given name.
.PP
For example, if a user wants to add a paragraph to every README file
that points to his web page, he could copy
\fI/usr/local/share/autoproject/all/all/all/README\fP to
\fI~/.autoproject/all/all/all/README\fP and make that change.
Of course, any file overridden in this way will not inherit updates
when the next version of \fBautoproject\fP is installed.
.PP
If a skeleton file contains any of these variables, \fBautoproject\fP
will substitute the corresponding value:
.TP
.B #NAME#
Program name in lower case.
.TP
.B #CAPNAME#
Program name in all caps.
.TP
.B #DESCRIPTION#
A short description of the program.
.TP
.B #AUTHOR#
Program author.
.TP
.B #EMAIL#
Author's email address.
.TP
.B #EEMAIL#
Author's email address with the `@' doubled (necessary in a .texinfo file).
.TP
.B #DATE#
Today's date, in this format: "November 24, 2001".
.TP
.B #ISODATE#
Today's date, in ISO 8601 format: "2001-11-24".
.TP
.B #YEAR#
The four digit year.
.PP
Note that these substitutions are made when autoproject runs.
Substitutions can also be made at program configuration or build time
by suitable makefile commands (for example, using the makefile
variable VERSION, or the output of \fBdate\fP(1)).
.PP
If you write a generally applicable skeleton file, such as a main
program for a language currently not supported, please consider
contributing it to \fBautoproject\fP.
.SH FILES
\fI$HOME/.autoproject\fP
.br
\fI/etc/autoproject\fP
.br
\fI/usr/local/share/autoproject\fP
.br
Directory trees containing skeleton files.
.SH "SEE ALSO"
.BR autoconf (1),
.BR automake (1),
.BR autogen (1),
.BR clig (1),
`Parsing Program Options with Argp' in `The GNU C Library Reference
Manual' (type `\fBinfo libc argp\fP')
.SH AUTHOR
James R. Van Zandt <jrv@vanzandt.mv.com>.
